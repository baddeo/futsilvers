/* Scroll to top of page on click */

$('#backtotop').on("click",function(){
      $(window).scrollTop(0);
});

/* If statement to display platform based on user input */
if ($(".platform").text() == "xbox") {
    document.getElementById("xboxlogo").style.display = "block";
}
else
if ($(".platform").text() == "playstation") {
    document.getElementById("playstationlogo").style.display = "block";
}
else
if ($(".platform").text() == "origin") {
    document.getElementById("originlogo").style.display = "block";
}


/* If statement to display stars based on value */
/* Review 1 */

if ($(".playerqualityvalue").text() == 1) {
    document.getElementById("yellowstar1").style.display = "inline-block";
    document.getElementById("graystar2").style.display = "inline-block";
    document.getElementById("graystar3").style.display = "inline-block";
    document.getElementById("graystar4").style.display = "inline-block";
    document.getElementById("graystar5").style.display = "inline-block";
}
else
if ($(".playerqualityvalue").text() == 2) {
    document.getElementById("yellowstar1").style.display = "inline-block";
    document.getElementById("yellowstar2").style.display = "inline-block";
    document.getElementById("graystar3").style.display = "inline-block";
    document.getElementById("graystar4").style.display = "inline-block";
    document.getElementById("graystar5").style.display = "inline-block";
}
else
if ($(".playerqualityvalue").text() == 3) {
    document.getElementById("yellowstar1").style.display = "inline-block";
    document.getElementById("yellowstar2").style.display = "inline-block";
    document.getElementById("yellowstar3").style.display = "inline-block";
    document.getElementById("graystar4").style.display = "inline-block";
    document.getElementById("graystar5").style.display = "inline-block";
}
else
if ($(".playerqualityvalue").text() == 4) {
    document.getElementById("yellowstar1").style.display = "inline-block";
    document.getElementById("yellowstar2").style.display = "inline-block";
    document.getElementById("yellowstar3").style.display = "inline-block";
    document.getElementById("yellowstar4").style.display = "inline-block";
    document.getElementById("graystar5").style.display = "inline-block";
}
else
if ($(".playerqualityvalue").text() == 5) {
    document.getElementById("yellowstar1").style.display = "inline-block";
    document.getElementById("yellowstar2").style.display = "inline-block";
    document.getElementById("yellowstar3").style.display = "inline-block";
    document.getElementById("yellowstar4").style.display = "inline-block";
    document.getElementById("yellowstar5").style.display = "inline-block";
}

/* Review 2 */

if ($(".playerqualityvalue2").text() == 1) {
    document.getElementById("yellowstar1review2").style.display = "inline-block";
    document.getElementById("graystar2review2").style.display = "inline-block";
    document.getElementById("graystar3review2").style.display = "inline-block";
    document.getElementById("graystar4review2").style.display = "inline-block";
    document.getElementById("graystar5review2").style.display = "inline-block";
}
else
if ($(".playerqualityvalue2").text() == 2) {
    document.getElementById("yellowstar1review2").style.display = "inline-block";
    document.getElementById("yellowstar2review2").style.display = "inline-block";
    document.getElementById("graystar3review2").style.display = "inline-block";
    document.getElementById("graystar4review2").style.display = "inline-block";
    document.getElementById("graystar5review2").style.display = "inline-block";
}
else
if ($(".playerqualityvalue2").text() == 3) {
    document.getElementById("yellowstar1review2").style.display = "inline-block";
    document.getElementById("yellowstar2review2").style.display = "inline-block";
    document.getElementById("yellowstar3review2").style.display = "inline-block";
    document.getElementById("graystar4review2").style.display = "inline-block";
    document.getElementById("graystar5review2").style.display = "inline-block";
}
else
if ($(".playerqualityvalue2").text() == 4) {
    document.getElementById("yellowstar1review2").style.display = "inline-block";
    document.getElementById("yellowstar2review2").style.display = "inline-block";
    document.getElementById("yellowstar3review2").style.display = "inline-block";
    document.getElementById("yellowstar4review2").style.display = "inline-block";
    document.getElementById("graystar5review2").style.display = "inline-block";
}
else
if ($(".playerqualityvalue2").text() == 5) {
    document.getElementById("yellowstar1review2").style.display = "inline-block";
    document.getElementById("yellowstar2review2").style.display = "inline-block";
    document.getElementById("yellowstar3review2").style.display = "inline-block";
    document.getElementById("yellowstar4review2").style.display = "inline-block";
    document.getElementById("yellowstar5review2").style.display = "inline-block";
}

/* Review 3 */

if ($(".playerqualityvalue3").text() == 1) {
    document.getElementById("yellowstar1review3").style.display = "inline-block";
    document.getElementById("graystar2review3").style.display = "inline-block";
    document.getElementById("graystar3review3").style.display = "inline-block";
    document.getElementById("graystar4review3").style.display = "inline-block";
    document.getElementById("graystar5review3").style.display = "inline-block";
}
else
if ($(".playerqualityvalue3").text() == 2) {
    document.getElementById("yellowstar1review3").style.display = "inline-block";
    document.getElementById("yellowstar2review3").style.display = "inline-block";
    document.getElementById("graystar3review3").style.display = "inline-block";
    document.getElementById("graystar4review3").style.display = "inline-block";
    document.getElementById("graystar5review3").style.display = "inline-block";
}
else
if ($(".playerqualityvalue3").text() == 3) {
    document.getElementById("yellowstar1review3").style.display = "inline-block";
    document.getElementById("yellowstar2review3").style.display = "inline-block";
    document.getElementById("yellowstar3review3").style.display = "inline-block";
    document.getElementById("graystar4review3").style.display = "inline-block";
    document.getElementById("graystar5review3").style.display = "inline-block";
}
else
if ($(".playerqualityvalue3").text() == 4) {
    document.getElementById("yellowstar1review3").style.display = "inline-block";
    document.getElementById("yellowstar2review3").style.display = "inline-block";
    document.getElementById("yellowstar3review3").style.display = "inline-block";
    document.getElementById("yellowstar4review3").style.display = "inline-block";
    document.getElementById("graystar5review3").style.display = "inline-block";
}
else
if ($(".playerqualityvalue3").text() == 5) {
    document.getElementById("yellowstar1review3").style.display = "inline-block";
    document.getElementById("yellowstar2review3").style.display = "inline-block";
    document.getElementById("yellowstar3review3").style.display = "inline-block";
    document.getElementById("yellowstar4review3").style.display = "inline-block";
    document.getElementById("yellowstar5review3").style.display = "inline-block";
}

/* Review 4 */

if ($(".playerqualityvalue4").text() == 1) {
    document.getElementById("yellowstar1review4").style.display = "inline-block";
    document.getElementById("graystar2review4").style.display = "inline-block";
    document.getElementById("graystar3review4").style.display = "inline-block";
    document.getElementById("graystar4review4").style.display = "inline-block";
    document.getElementById("graystar5review4").style.display = "inline-block";
}
else
if ($(".playerqualityvalue4").text() == 2) {
    document.getElementById("yellowstar1review4").style.display = "inline-block";
    document.getElementById("yellowstar2review4").style.display = "inline-block";
    document.getElementById("graystar3review4").style.display = "inline-block";
    document.getElementById("graystar4review4").style.display = "inline-block";
    document.getElementById("graystar5review4").style.display = "inline-block";
}
else
if ($(".playerqualityvalue4").text() == 3) {
    document.getElementById("yellowstar1review4").style.display = "inline-block";
    document.getElementById("yellowstar2review4").style.display = "inline-block";
    document.getElementById("yellowstar3review4").style.display = "inline-block";
    document.getElementById("graystar4review4").style.display = "inline-block";
    document.getElementById("graystar5review4").style.display = "inline-block";
}
else
if ($(".playerqualityvalue4").text() == 4) {
    document.getElementById("yellowstar1review4").style.display = "inline-block";
    document.getElementById("yellowstar2review4").style.display = "inline-block";
    document.getElementById("yellowstar3review4").style.display = "inline-block";
    document.getElementById("yellowstar4review4").style.display = "inline-block";
    document.getElementById("graystar5review4").style.display = "inline-block";
}
else
if ($(".playerqualityvalue4").text() == 5) {
    document.getElementById("yellowstar1review4").style.display = "inline-block";
    document.getElementById("yellowstar2review4").style.display = "inline-block";
    document.getElementById("yellowstar3review4").style.display = "inline-block";
    document.getElementById("yellowstar4review4").style.display = "inline-block";
    document.getElementById("yellowstar5review4").style.display = "inline-block";
}

/* Open Login div on click */

$(document).ready(function() {
  $("#register").click(function() {
     var h = $("body").height() + 'px';
     $("#black_overlay").css({"height":h,"visibility":"visible"});
     $(".logindiv").css('display','block');
  });

/* Open Register div on click */

  $(document).ready(function() {
  $("#login").click(function() {
     var h = $("body").height() + 'px';
     $("#black_overlay").css({"height":h,"visibility":"visible"});
     $(".registerdiv").css('display','block');
  });

/* Close Register div on X click */

    $(".close, .registerbutton").click(function() {
     $(".registerdiv").css('display','none');
     $("#black_overlay").css("visibility","hidden");
  });
});

/* Close Login div on X click */

  $(".close, .loginbutton").click(function() {
     $(".logindiv").css('display','none');
     $("#black_overlay").css("visibility","hidden");
  });
});

if ($("paceheader").css('background-color') === "#5fc042") {
    document.getElementById("reviewpaceheader").style.backgroundColor = "#5fc042";
}

/* Change background colour of div depending on number */
/* Headers */

if ($(".pacenumber").text() <= 59) {
    document.getElementById("paceheader").style.backgroundColor = "#de2323";
} 
else
if ($(".pacenumber").text() < 80) {
    document.getElementById("paceheader").style.backgroundColor = "#edb611";
}
else
if ($(".pacenumber").text() >= 80) {
    document.getElementById("paceheader").style.backgroundColor = "#5fc042";
}



if ($(".dribblingnumber").text() <= 59) {
    document.getElementById("dribblingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".dribblingnumber").text() < 80) {
    document.getElementById("dribblingheader").style.backgroundColor = "#edb611";
}
else
if ($(".dribblingnumber").text() >= 80) {
    document.getElementById("dribblingheader").style.backgroundColor = "#5fc042";
}



if ($(".shootingnumber").text() <= 59) {
    document.getElementById("shootingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".shootingnumber").text() < 80) {
    document.getElementById("shootingheader").style.backgroundColor = "#edb611";
}
else
if ($(".shootingnumber").text() >= 80) {
    document.getElementById("shootingheader").style.backgroundColor = "#5fc042";
}



if ($(".defendingnumber").text() <= 59) {
    document.getElementById("defendingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".defendingnumber").text() < 80) {
    document.getElementById("defendingheader").style.backgroundColor = "#edb611";
}
else
if ($(".defendingnumber").text() >= 80) {
    document.getElementById("defendingheader").style.backgroundColor = "#5fc042";
}



if ($(".passingnumber").text() <= 59) {
    document.getElementById("passingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".passingnumber").text() < 80) {
    document.getElementById("passingheader").style.backgroundColor = "#edb611";
}
else
if ($(".passingnumber").text() >= 80) {
    document.getElementById("passingheader").style.backgroundColor = "#5fc042";
}



if ($(".physicalnumber").text() <= 59) {
    document.getElementById("physicalheader").style.backgroundColor = "#de2323";
} 
else
if ($(".physicalnumber").text() < 80) {
    document.getElementById("physicalheader").style.backgroundColor = "#edb611";
}
else
if ($(".physicalnumber").text() >= 80) {
    document.getElementById("physicalheader").style.backgroundColor = "#5fc042";
}

/* Pace */

if ($(".accelerationnumber").text() <= 59) {
    document.getElementById("accelerationdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".accelerationnumber").text() < 80) {
    document.getElementById("accelerationdiv").style.backgroundColor = "#edb611";
}
else
if ($(".accelerationnumber").text() >= 80) {
    document.getElementById("accelerationdiv").style.backgroundColor = "#5fc042";
}



if ($(".sprintspeednumber").text() <= 59) {
    document.getElementById("sprintspeeddiv").style.backgroundColor = "#de2323";
} 
else
if ($(".sprintspeednumber").text() < 80) {
    document.getElementById("sprintspeeddiv").style.backgroundColor = "#edb611";
}
else
if ($(".sprintspeednumber").text() >= 80) {
    document.getElementById("sprintspeeddiv").style.backgroundColor = "#5fc042";
}

/* Dribbling */

if ($(".agilitynumber").text() <= 59) {
    document.getElementById("agilitydiv").style.backgroundColor = "#de2323";
} 
else
if ($(".agilitynumber").text() < 80) {
    document.getElementById("agilitydiv").style.backgroundColor = "#edb611";
}
else
if ($(".agilitynumber").text() >= 80) {
    document.getElementById("agilitydiv").style.backgroundColor = "#5fc042";
}



if ($(".balancenumber").text() <= 59) {
    document.getElementById("balancediv").style.backgroundColor = "#de2323";
} 
else
if ($(".balancenumber").text() < 80) {
    document.getElementById("balancediv").style.backgroundColor = "#edb611";
}
else
if ($(".balancenumber").text() >= 80) {
    document.getElementById("balancediv").style.backgroundColor = "#5fc042";
}



if ($(".reactionsnumber").text() <= 59) {
    document.getElementById("reactionsdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reactionsnumber").text() < 80) {
    document.getElementById("reactionsdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reactionsnumber").text() >= 80) {
    document.getElementById("reactionsdiv").style.backgroundColor = "#5fc042";
}



if ($(".ballcontrolnumber").text() <= 59) {
    document.getElementById("ballcontroldiv").style.backgroundColor = "#de2323";
} 
else
if ($(".ballcontrolnumber").text() < 80) {
    document.getElementById("ballcontroldiv").style.backgroundColor = "#edb611";
}
else
if ($(".ballcontrolnumber").text() >= 80) {
    document.getElementById("ballcontroldiv").style.backgroundColor = "#5fc042";
}



if ($(".dribblingstatnumber").text() <= 59) {
    document.getElementById("dribblingstatdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".dribblingstatnumber").text() < 80) {
    document.getElementById("dribblingstatdiv").style.backgroundColor = "#edb611";
}
else
if ($(".dribblingstatnumber").text() >= 80) {
    document.getElementById("dribblingstatdiv").style.backgroundColor = "#5fc042";
}

/* Shooting */

if ($(".attpositioningnumber").text() <= 59) {
    document.getElementById("attpositioningdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".attpositioningnumber").text() < 80) {
    document.getElementById("attpositioningdiv").style.backgroundColor = "#edb611";
}
else
if ($(".attpositioningnumber").text() >= 80) {
    document.getElementById("attpositioningdiv").style.backgroundColor = "#5fc042";
}



if ($(".finishingnumber").text() <= 59) {
    document.getElementById("finishingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".finishingnumber").text() < 80) {
    document.getElementById("finishingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".finishingnumber").text() >= 80) {
    document.getElementById("finishingdiv").style.backgroundColor = "#5fc042";
}



if ($(".shotpowernumber").text() <= 59) {
    document.getElementById("shotpowerdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".shotpowernumber").text() < 80) {
    document.getElementById("shotpowerdiv").style.backgroundColor = "#edb611";
}
else
if ($(".shotpowernumber").text() >= 80) {
    document.getElementById("shotpowerdiv").style.backgroundColor = "#5fc042";
}



if ($(".longshotsnumber").text() <= 59) {
    document.getElementById("longshotsdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".longshotsnumber").text() < 80) {
    document.getElementById("longshotsdiv").style.backgroundColor = "#edb611";
}
else
if ($(".longshotsnumber").text() >= 80) {
    document.getElementById("longshotsdiv").style.backgroundColor = "#5fc042";
}



if ($(".volleysnumber").text() <= 59) {
    document.getElementById("volleysdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".volleysnumber").text() < 80) {
    document.getElementById("volleysdiv").style.backgroundColor = "#edb611";
}
else
if ($(".volleysnumber").text() >= 80) {
    document.getElementById("volleysdiv").style.backgroundColor = "#5fc042";
}



if ($(".penaltiesnumber").text() <= 59) {
    document.getElementById("penaltiesdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".penaltiesnumber").text() < 80) {
    document.getElementById("penaltiesdiv").style.backgroundColor = "#edb611";
}
else
if ($(".penaltiesnumber").text() >= 80) {
    document.getElementById("penaltiesdiv").style.backgroundColor = "#5fc042";
}

/* Defending */

if ($(".interceptionsnumber").text() <= 59) {
    document.getElementById("interceptionsdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".interceptionsnumber").text() < 80) {
    document.getElementById("interceptionsdiv").style.backgroundColor = "#edb611";
}
else
if ($(".interceptionsnumber").text() >= 80) {
    document.getElementById("interceptionsdiv").style.backgroundColor = "#5fc042";
}



if ($(".headingaccnumber").text() <= 59) {
    document.getElementById("headingaccdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".headingaccnumber").text() < 80) {
    document.getElementById("headingaccdiv").style.backgroundColor = "#edb611";
}
else
if ($(".headingaccnumber").text() >= 80) {
    document.getElementById("headingaccdiv").style.backgroundColor = "#5fc042";
}



if ($(".markingnumber").text() <= 59) {
    document.getElementById("markingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".markingnumber").text() < 80) {
    document.getElementById("markingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".markingnumber").text() >= 80) {
    document.getElementById("markingdiv").style.backgroundColor = "#5fc042";
}



if ($(".standingtacklenumber").text() <= 59) {
    document.getElementById("standingtacklediv").style.backgroundColor = "#de2323";
} 
else
if ($(".standingtacklenumber").text() < 80) {
    document.getElementById("standingtacklediv").style.backgroundColor = "#edb611";
}
else
if ($(".standingtacklenumber").text() >= 80) {
    document.getElementById("standingtacklediv").style.backgroundColor = "#5fc042";
}



if ($(".slidingtacklenumber").text() <= 59) {
    document.getElementById("slidingtacklediv").style.backgroundColor = "#de2323";
} 
else
if ($(".slidingtacklenumber").text() < 80) {
    document.getElementById("slidingtacklediv").style.backgroundColor = "#edb611";
}
else
if ($(".slidingtacklenumber").text() >= 80) {
    document.getElementById("slidingtacklediv").style.backgroundColor = "#5fc042";
}

/* Passing */

if ($(".visionnumber").text() <= 59) {
    document.getElementById("visiondiv").style.backgroundColor = "#de2323";
} 
else
if ($(".visionnumber").text() < 80) {
    document.getElementById("visiondiv").style.backgroundColor = "#edb611";
}
else
if ($(".visionnumber").text() >= 80) {
    document.getElementById("visiondiv").style.backgroundColor = "#5fc042";
}



if ($(".crossingnumber").text() <= 59) {
    document.getElementById("crossingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".crossingnumber").text() < 80) {
    document.getElementById("crossingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".crossingnumber").text() >= 80) {
    document.getElementById("crossingdiv").style.backgroundColor = "#5fc042";
}



if ($(".freekickaccnumber").text() <= 59) {
    document.getElementById("freekickaccdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".freekickaccnumber").text() < 80) {
    document.getElementById("freekickaccdiv").style.backgroundColor = "#edb611";
}
else
if ($(".freekickaccnumber").text() >= 80) {
    document.getElementById("freekickaccdiv").style.backgroundColor = "#5fc042";
}



if ($(".shortpassingnumber").text() <= 59) {
    document.getElementById("shortpassingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".shortpassingnumber").text() < 80) {
    document.getElementById("shortpassingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".shortpassingnumber").text() >= 80) {
    document.getElementById("shortpassingdiv").style.backgroundColor = "#5fc042";
}



if ($(".longpassingnumber").text() <= 59) {
    document.getElementById("longpassingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".longpassingnumber").text() < 80) {
    document.getElementById("longpassingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".longpassingnumber").text() >= 80) {
    document.getElementById("longpassingdiv").style.backgroundColor = "#5fc042";
}



if ($(".curvenumber").text() <= 59) {
    document.getElementById("curvediv").style.backgroundColor = "#de2323";
} 
else
if ($(".curvenumber").text() < 80) {
    document.getElementById("curvediv").style.backgroundColor = "#edb611";
}
else
if ($(".curvenumber").text() >= 80) {
    document.getElementById("curvediv").style.backgroundColor = "#5fc042";
}

/* Physical */

if ($(".jumpingnumber").text() <= 59) {
    document.getElementById("jumpingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".jumpingnumber").text() < 80) {
    document.getElementById("jumpingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".jumpingnumber").text() >= 80) {
    document.getElementById("jumpingdiv").style.backgroundColor = "#5fc042";
}



if ($(".staminanumber").text() <= 59) {
    document.getElementById("staminadiv").style.backgroundColor = "#de2323";
} 
else
if ($(".staminanumber").text() < 80) {
    document.getElementById("staminadiv").style.backgroundColor = "#edb611";
}
else
if ($(".staminanumber").text() >= 80) {
    document.getElementById("staminadiv").style.backgroundColor = "#5fc042";
}



if ($(".strengthnumber").text() <= 59) {
    document.getElementById("strengthdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".strengthnumber").text() < 80) {
    document.getElementById("strengthdiv").style.backgroundColor = "#edb611";
}
else
if ($(".strengthnumber").text() >= 80) {
    document.getElementById("strengthdiv").style.backgroundColor = "#5fc042";
}



if ($(".aggressionnumber").text() <= 59) {
    document.getElementById("aggressiondiv").style.backgroundColor = "#de2323";
} 
else
if ($(".aggressionnumber").text() < 80) {
    document.getElementById("aggressiondiv").style.backgroundColor = "#edb611";
}
else
if ($(".aggressionnumber").text() >= 80) {
    document.getElementById("aggressiondiv").style.backgroundColor = "#5fc042";
}


