<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="favicon" type="img/ico" href="favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
    </head>
    
    <body>
        <header>
            <div id="left" style="height: 115px;">
                <img id="icon" src="img/youtube icon.png">
                <img id="icon" src="img/facebook icon.png">
                <img id="icon" src="img/twitter icon.png">
            </div>
            <div id="center" style="height: 115px;"><img id="logo" src="img/futsilverslogo.png"></div>
            <div id="right" style="height: 115px;">
                    <div id="register">LOGIN</div>
                    <div id="login">REGISTER</div>
            </div>



            <div id="holder">

                <!--[if lt IE 8]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
                <![endif]-->

                <!-- Add your site or application content here -->
               

               <ul id="nav">
                    <li class="dropdown"><a class="dropbtn" href="index.php">HOME</a></li>
                    <li class="dropdown"><a class="dropbtn" href="#players">PLAYERS</a>
                    <div class="dropdown-content">
                        <a href="#">SILVER PLAYERS</a>
                        <a href="#">PERFECT LINKS</a>
                        <a href="#">SILVER GEMS</a>
                        <a href="#">5 STAR SKILLERS</a>
                        <a href="#">RANDOM SILVER</a>
                    </div>
                    </li>
                    <li class="dropdown"><a class="dropbtn" href="#reviews">REVIEWS</a>
                    <div class="dropdown-content">
                        <a href="#">TOP REVIEWS</a>
                        <a href="#">POPULAR REVIEWS</a>
                        <a href="#">RECENT REVIEWS</a>
                    </div>
                    </li>
                    <li class="dropdown"><a class="dropbtn" href="#totwsilvers">TOTW SILVERS</a>
                    <div class="dropdown-bar">
                        <div id="totwsilversbar">
                            <img id="thumbnail" src="img/TOTW/TOTW1.png">
                            <img id="thumbnail" src="img/TOTW/TOTW2.png">
                            <img id="thumbnail" src="img/TOTW/TOTW3.png">
                            <img id="thumbnail" src="img/TOTW/TOTW4.png">
                        </div>
                        <div class="viewallbutton1">VIEW ALL</div>
                    </div>
                    </li>
                    <li class="dropdown"><a class="dropbtn" href="#tipsandtricks">TIPS & TRICKS</a>
                    <div class="dropdown-bar">
                        <div id="tipstricksbar">
                            <img id="thumbnail" src="img/TipsTricks/chooseplayers.png">
                            <img id="thumbnail" src="img/TipsTricks/whentobuytotw.png">
                            <img id="thumbnail" src="img/TipsTricks/surviveseasons.png">
                            <img id="thumbnail" src="img/TipsTricks/silverinstructions.png">
                        </div>
                        <div class="viewallbutton2">VIEW ALL</div>
                    </div>
                    </li>
                </ul>

            </div>

                <div id="spacer"></div>

                <div id="black_overlay" style="width: 100%;"> </div>
                <div class="logindiv">
                    <div class="close"><img id="closeimg" src="img/close.png"></div>
                    <div class="loginheader">LOGIN</div>
                    <form><input type="text" name="firstname" id="username" placeholder="Username"></form><br>
                    <form><input type="text" name="firstname" id="password" placeholder="Password"></form><br>
                    <div class="passwordcheckbox">
                        <input type="checkbox" class="checkbox">
                        <div class="checkboxtext">Keep me logged in?</div>
                    </div>
                    <div class="forgotpassword">Forgot your password?</div><br><br>
                    <div class="loginbutton">LOGIN</div>
                </div>
                
                <div id="black_overlay" style="width: 100%;"> </div>
                <div class="registerdiv">
                    <div class="close"><img id="closeimg" src="img/close.png"></div>
                    <div class="loginheader">REGISTER</div>
                    <form><input type="text" name="firstname" id="email" placeholder="Email"></form><br>
                    <form><input type="text" name="firstname" id="username" placeholder="Choose Username"></form><br>
                    <form><input type="text" name="firstname" id="password" placeholder="Password"></form><br>
                    <form><input type="text" name="firstname" id="confirmpassword" placeholder="Confirm Password"></form><br>
                    <div class="registerbutton">REGISTER</div>
                </div>

        </header>
