<?php include 'header.php';?>

        <div id="searchbox">
          SEARCH FOR SILVER PLAYERS TO VIEW STATS AND REVIEWS
          <hr>
            <form>
                <input type="text" name="firstname" id="search" placeholder="SEARCH FOR SILVER PLAYERS"><br>
            </form>
        </div>

        <div id="thumbnails">
            <img class="largethumbnail" src="img/TOTW/TOTW1.png">
            <img class="largethumbnail" src="img/TOTW/TOTW2.png">
        </div>

        <div id="reviews">
            <div id="topreviews">
                <div id="topreviewsheader">TOP REVIEWS</div>
                <a href="playerpage.php">
                    <div class="topreviewstab1">
                        <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Traore.png"></div>
                        <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Chelsea.png"></div>
                        <div class="topreviewsplayername">Bertrand Traoré</div>
                        <div class="topreviewsovrrating">74</div>
                        <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/BurkinaFaso.png"></div>
                    </div>
                </a>
                <div class="topreviewstab2">
                    <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Helland.png"></div>
                    <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Rosenborg.png"></div>
                    <div class="topreviewsplayername">Pål André Helland</div>
                    <div class="topreviewsovrrating">74</div>
                    <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/Norway.png"></div>
                </div>
                <div class="topreviewstab3">
                    <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Medunjanin.png"></div>
                    <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Deportivo.png"></div>
                    <div class="topreviewsplayername">Haris Medunjanin</div>
                    <div class="topreviewsovrrating">74</div>
                    <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/Bosnia.png"></div>
                </div>
                <div class="topreviewstab4">
                    <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Doumbia.png"></div>
                    <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Toulouse.png"></div>
                    <div class="topreviewsplayername">Tongo Doumbia</div>
                    <div class="topreviewsovrrating">74</div>
                    <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/Mali.png"></div>
                </div>
                <div class="topreviewstab5">
                    <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Sinclair.png"></div>
                    <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/AstonVilla.png"></div>
                    <div class="topreviewsplayername">Scott Sinclair</div>
                    <div class="topreviewsovrrating">74</div>
                    <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/England.png"></div>
                </div>
            </div>
            <div id="recentreviews">
                <div id="recentreviewsheader">RECENT REVIEWS</div>
                <div class="recentreviewstab1">
                    <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Medunjanin.png"></div>
                    <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Deportivo.png"></div>
                    <div class="topreviewsplayername">Haris Medunjanin</div>
                    <div class="topreviewsovrrating">74</div>
                    <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/Bosnia.png"></div>
                </div>
                <div class="recentreviewstab2">
                    <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Traore.png"></div>
                        <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Chelsea.png"></div>
                        <div class="topreviewsplayername">Bertrand Traoré</div>
                        <div class="topreviewsovrrating">74</div>
                        <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/BurkinaFaso.png"></div>
                </div>
                <a href="playerpage.php">
                    <div class="recentreviewstab3">
                        <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Traore.png"></div>
                        <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Chelsea.png"></div>
                        <div class="topreviewsplayername">Bertrand Traoré</div>
                        <div class="topreviewsovrrating">74</div>
                        <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/BurkinaFaso.png"></div>
                    </div>
                </a>
                <div class="recentreviewstab4">
                   <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Helland.png"></div>
                    <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Rosenborg.png"></div>
                    <div class="topreviewsplayername">Pål André Helland</div>
                    <div class="topreviewsovrrating">74</div>
                    <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/Norway.png"></div>
                </div>
                <div class="recentreviewstab5">
                    <div class="topreviewsplayerimgdiv"><img id="topreviewsplayerimg" src="img/photo/Doumbia.png"></div>
                    <div class="topreviewsbadgediv"><img id="topreviewsbadge" src="img/badge/Toulouse.png"></div>
                    <div class="topreviewsplayername">Tongo Doumbia</div>
                    <div class="topreviewsovrrating">74</div>
                    <div class="topreviewsflagdiv"><img id="topreviewsflag" src="img/flag/Mali.png"></div>
                </div>
            </div>
        </div>

<?php include 'footer.php';?>
