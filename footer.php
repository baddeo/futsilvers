
        <footer>
            <div id="leftfooter">
                <img id="iconfooter" src="img/youtube icon.png">
                <img id="iconfooter" src="img/facebook icon.png">
                <img id="iconfooter" src="img/twitter icon.png">
            </div>
            <div id="centerfooter">© 2016 Futsilvers.com - All FIFA assets are property of EA Sports</div>
            <div id="rightfooter">
                <div id="about">About</div><div id="footerspacer">|</div><div id="backtotop">Back to top</div>
            </div>
        </footer>


        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
